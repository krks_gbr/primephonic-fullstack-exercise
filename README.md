## How to run

The easiest way to run the app is using `docker-compose`.
Just use `docker-compose up` and visit `localhost:5000`.


Alternatively, do the following:

### Backend

```
cd backend
yarn
yarn build
PORT=3000 yarn start
```

### Frontend

``` 
cd frontend
yarn 
yarn build
yarn serve
```

Now visit `localhost:5000`.


## Notes

### Services
The backend and the frontend are organized as two separate projects, to
decouple them. A general assumption is that system has to fit in a
microservices architecture.

### Backend
**Tests**

The backend has some unit tests.
You can run them by `docker-compose exec backend yarn jest` (from project root)
or `yarn test` (from the backend directory).

**Data access layer**

It is assumed that some kind of an abstraction layer is provided for accessing data.
This layer is stubbed in `src/Data.ts`. Because this is assumed to be a given,
no tests are written for this code.

### Possible improvements
Instead of polling via HTTP at an interval, the frontend could receive new data real-time
through a websocket connection.
