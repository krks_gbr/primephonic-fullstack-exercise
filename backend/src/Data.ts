export interface Event {
  userId: string;
  trackId: string;
  label: string;
  streamStartedOn: number;
  secondsStreamed: number;
}

const users = ["user1", "user2", "user3"];
const trackIds = ["track1", "track2", "track3"];
const labels = ["label1", "label2", "label3"];

function pickRandom<T>(arr: T[]) {
  const i = Math.floor(Math.random() * arr.length);
  return arr[i];
}

function lerp(a: number, b: number, t: number) {
  return a + (b - a) * t;
}

function generateEvent(): Event {
  const twoSeconds = 2;
  const halfHour = 30 * 60;
  const secondsStreamed = Math.floor(lerp(halfHour, twoSeconds, Math.random()));
  const currentSeconds = new Date().getTime() / 1000;
  return {
    userId: pickRandom(users),
    trackId: pickRandom(trackIds),
    label: pickRandom(labels),
    streamStartedOn: currentSeconds - secondsStreamed,
    secondsStreamed
  };
}

export interface EventFilter {
  streamStartedOn: {
    eq?: number;
    lt?: number;
    gt?: number;
  };
}

export interface DataSource {
  list: (filter?: EventFilter) => Promise<Event[]>;
}

export namespace DataSource {
  export async function connect(): Promise<DataSource> {
    let events: Event[] = [];

    setInterval(() => {
      const n = 4 * Math.random() + 1;
      const newEvents = Array.from({ length: n }).map(_ => generateEvent());
      events = events.concat(newEvents);
    }, 10000);

    return {
      async list(filter?: EventFilter) {
        if (!filter) {
          return events;
        }
        const { eq, lt, gt } = filter.streamStartedOn;
        if (eq) {
          return events.filter(e => eq === e.streamStartedOn);
        } else if (lt) {
          return events.filter(e => lt > e.streamStartedOn);
        } else if (gt) {
          return events.filter(e => gt < e.streamStartedOn);
        }
        return events;
      }
    };
  }
}
