import { Service } from "./Service";
import { DataSource } from "./Data";

if (process.env.PORT) {
  const port = process.env.PORT;
  (async function() {
    try {
      const datasource = await DataSource.connect();
      const app = Service.create({
        datasource
      });
      Service.run({ port }, app);
    } catch (e) {
      throw new Error("unable to connect to data source");
    }
  })();
} else {
  throw new Error("Please export PORT in your environment");
}
