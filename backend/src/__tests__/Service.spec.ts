import * as request from "supertest";
import { Service } from "../Service";
import { Event, EventFilter } from "../Data";

function mockDataSource(result: Event[]) {
  return {
    list: jest.fn(async (filter?: EventFilter) => {
      return result;
    })
  };
}

function mockService({
  result,
  currentSeconds
}: {
  result: Event[];
  currentSeconds?: number;
}) {
  const datasource = mockDataSource(result);
  return {
    service: Service.create({
      datasource,
      getCurrentSeconds: currentSeconds ? () => currentSeconds : undefined
    }),
    datasource
  };
}

describe("service", () => {
  const mockEvent: Event = {
    userId: "testUserId",
    trackId: "testTrackId",
    label: "testlabel",
    streamStartedOn: 0,
    secondsStreamed: 0
  };
  describe("GET /usage without query paramaters", () => {
    const { service } = mockService({
      result: [mockEvent]
    });
    it("returns all events", () => {
      return request(service)
        .get("/usage")
        .expect(200, [mockEvent]);
    });
  });
  describe("GET /usage?from=1", () => {
    const { service, datasource } = mockService({
      result: [mockEvent],
      currentSeconds: 10
    });
    it("correctly calls database and returns the result", async () => {
      await request(service)
        .get("/usage?from=1")
        .expect(200, [mockEvent]);

      expect(datasource.list.mock.calls[0][0]).toEqual({
        streamStartedOn: { gt: 1 }
      });
    });
  });

  describe("GET /usage with future time", () => {
    const { service } = mockService({
      result: [mockEvent],
      currentSeconds: 10
    });
    it("responds with 400 and empty body", () => {
      return request(service)
        .get("/usage?from=11")
        .expect(400, {});
    });
  });

  describe("GET /usage with invalid parameter", () => {
    const { service } = mockService({
      result: [mockEvent],
      currentSeconds: 10
    });
    it("responds with 400 and an error message", () => {
      return request(service)
        .get("/usage?from=notanumber")
        .expect(400, "'from' must be a number");
    });
  });
});
