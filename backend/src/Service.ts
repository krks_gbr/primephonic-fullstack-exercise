import * as Express from "express";
import { DataSource } from "./Data";
import * as cors from "cors";

export namespace Service {
  export interface Configuration {
    port: string;
  }

  interface QueryParams {
    from?: string;
  }

  interface Context {
    datasource: DataSource;
    getCurrentSeconds?: () => number;
  }

  const defaultCurrentSecondsImpl = () => new Date().getTime() / 1000;

  export function create(ctx: Context) {
    const { getCurrentSeconds = defaultCurrentSecondsImpl } = ctx;
    const app = Express();
    app.use(cors());
    app.get("/usage", async (req, res) => {
      const q = req.query as QueryParams;
      if (q.from) {
        const before = parseInt(q.from);
        if (isNaN(before)) {
          return res.status(400).send("'from' must be a number");
        }
        const now = getCurrentSeconds();
        if (before > now) {
          return res.sendStatus(400);
        }

        const result = await ctx.datasource.list({
          streamStartedOn: { gt: before }
        });
        return res.json(result);
      }

      res.json(await ctx.datasource.list());
    });

    return app;
  }

  export function run(conf: Configuration, app: Express.Application) {
    app.listen(conf.port, () => {
      console.log(`app is listening on ${process.env.PORT}`);
    });
  }
}
