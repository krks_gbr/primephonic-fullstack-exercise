import Vue from "vue";
import Router from "vue-router";
import Stats from "./views/Stats.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Stats
    }
  ]
});
